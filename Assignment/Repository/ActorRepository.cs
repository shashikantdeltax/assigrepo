﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Assignment
{
    public class ActorRepository
    {
        private static readonly List<Actor> _listActor = new List<Actor>();

        public Actor AddNew(Actor obje)
        {
            _listActor.Add(obje);
            return obje;
        }

        public List<Actor> GetAll()
        {
            return _listActor;
        }

        public Actor GetById(int Id)
        {
            return _listActor[Id];
        }
    }
}
