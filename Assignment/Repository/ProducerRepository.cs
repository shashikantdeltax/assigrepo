﻿
using System.Collections.Generic;


namespace Assignment
{
    public class ProducerRepository
    {
        private static readonly List<Producer> _listProducer = new List<Producer>();

       
        public Producer AddNew(Producer obje)
        {
            _listProducer.Add(obje);
            return obje;
        }

        public List<Producer> GetAll()
        {
            return _listProducer;
        }

        public Producer GetById(int Id)
        {
            return _listProducer[Id];
        }
    }
}
