﻿using System;
using System.Collections.Generic;


namespace Assignment
{
    public class MovieRepository
    {
        
        private static readonly List<Movie> _listMovie = new List<Movie>();

       
        public Movie AddNew(Movie obje)
        {
           _listMovie.Add(obje);
            return obje;
        }

        public Movie GetById(int Id)
        {
            return _listMovie[Id];
        }

        public List<Movie> GetAll()
        {
            return _listMovie;
        }

        public Boolean Delete(string _movieName)
        {
            return _listMovie.Remove(_listMovie.Find(x => x.Name.Contains(_movieName)));
        }
    }
}
