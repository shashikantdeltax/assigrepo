﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Assignment
{
    public class IMDB
    {
       
        MovieRepository _movieRepository =new  MovieRepository();
        ProducerRepository _producerRepository = new ProducerRepository();
        ActorRepository _actorRepository = new ActorRepository();

        public void AddMovies(string _nameOfMovie, int _yearOfRelease,string _plotOfMovie,List<Actor> _list, int value)
        {
            _movieRepository.AddNew(new Movie() { Name = _nameOfMovie, YearOfRelease = _yearOfRelease, Plot = _plotOfMovie, Actors = _list, Producer = _producerRepository.GetById(value - 1) });
        }

        public Boolean RemoveMovie(string _movieName)
        {
            return _movieRepository.Delete(_movieName);
        }

        public Boolean ListMovie()
        {
            if (_movieRepository.GetAll().Count() == 0) {  return true;
            }
            else
            {
                return false;
            }
        }



        public void AddActors(String _nameOfActor, DateTime _dateOfBActor)
        {
            _actorRepository.AddNew(new Actor() { Name = _nameOfActor, Dob = _dateOfBActor });
        }



        public void AddProducer(string _nameOfProducer, DateTime _dateOfBProducer)
        {
            _producerRepository.AddNew(new Producer() { Name = _nameOfProducer, Dob = _dateOfBProducer });
        }


        public void ChooseActor(List<Actor> _list)
        {
            //_list = new List<Actor>();
            _list.Clear();
            String input = Console.ReadLine();
            string[] tokens = input.Split(' ');
            int[] numbers = Array.ConvertAll(tokens, int.Parse);
            foreach (var num in numbers)
            {
                if(num>0 && num<= _actorRepository.GetAll().Count())
                _list.Add(_actorRepository.GetById(num - 1));
            }
            if (numbers.Length != _list.Count)
            {
                Console.WriteLine("Select Actor from the list only:");
                ChooseActor(_list);
            }
        }

        public Boolean ValidateName(string Name)
        {
            Regex regex = new Regex(@"^(?:[aA-zZ]+[\s\.]?[aA-zZ]+)+$");
            return regex.IsMatch(Name);
        }
    }
}
