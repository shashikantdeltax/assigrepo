﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            var _imdb = new IMDB();
            MovieRepository _movieRepository = new MovieRepository();
            ProducerRepository _producerRepository = new ProducerRepository();
            ActorRepository _actorRepository = new ActorRepository();

            do
            {
                Console.WriteLine("//**********************************************\\");
                Console.WriteLine("  1  List Movies");
                Console.WriteLine("  2  Add Movie");
                Console.WriteLine("  3  Add Actor");
                Console.WriteLine("  4  Add Producer");
                Console.WriteLine("  5  Delete Movie");
                Console.WriteLine("  6  Exit");
                Console.WriteLine("//**********************************************\\");

                Console.Write("\nEnter a valid key:=> ");
                var _choice = Convert.ToInt32(Console.ReadLine());
            
                
                switch (_choice)
                {
                    case 1:
                        {
                            Console.WriteLine("----------------------------------------------");
                            Console.WriteLine("You entered 'List Movies'");
                            Console.WriteLine("==============================================\n\n");
                            if (_imdb.ListMovie())
                            {
                                Console.WriteLine("There are 0 Movies to display, ADD MOVIE\n\n");
                            }
                            else
                            {
                                Console.WriteLine("There are total " + _movieRepository.GetAll().Count + " Movie(s) in the list");
                                foreach (var list in _movieRepository.GetAll())
                                {
                                    Console.WriteLine("==============================================");
                                    Console.WriteLine("  Movie Name: " + list.Name + "\n  Year of Release: " + list.YearOfRelease + "\n  Plot of the Movie: " + list.Plot + "\n  Actors in the movie are :");
                                    foreach (var actorlist in list.Actors) Console.WriteLine("    * " + actorlist.Name);
                                    Console.WriteLine("\n  Produce name: " + list.Producer.Name);
                                    Console.WriteLine("==============================================\n\n");
                                }
                            }
                            break;
                        }

                    case 2:
                        {
                            Console.WriteLine("----------------------------------------------");
                            Console.WriteLine("You entered 'Add a Movie'");
                            Console.WriteLine("==============================================\n\n");
                    
                            if (_actorRepository.GetAll().Count == 0 || _producerRepository.GetAll().Count == 0)
                            {
                                Console.WriteLine("\n==>> Add actors and producers to the list first and then go for movie:( Atleast one actor and at most one producer is needed <<==\n\n");
                            }
                            else
                            {

                                Console.Write("Enter Movie Name: ");
                                var _nameOfMovie = Console.ReadLine();
                                Console.Write("Enter Year Of Release: ");
                                int _yearOfRelease = Convert.ToInt32(Console.ReadLine());
                                while (!(_yearOfRelease > 1900 && _yearOfRelease <= 2020))
                                {
                                    Console.Write("Enter valid year b/w (1900-2020): ");
                                    _yearOfRelease = Convert.ToInt32(Console.ReadLine());
                                }

                                Console.Write("Enter the Plot of Movie: ");
                                var _plotOfMovie = Console.ReadLine();




                                Console.WriteLine("Choose actor(s) at least one: ");
                                var i = 0;
                                foreach (var list in _actorRepository.GetAll())
                                {
                                    Console.Write(++i + "-> " + list.Name + ", ");
                                }
                                Console.WriteLine();


                                List<Actor> _list = new List<Actor>();
                                _imdb.ChooseActor(_list);





                                Console.WriteLine("Choose any one Producer for the movie: ");
                                i = 0;
                                foreach (var list in _producerRepository.GetAll())
                                {
                                    Console.Write(++i + "-> " + list.Name + ", ");
                                }
                                Console.WriteLine();


                                int value = Convert.ToInt32(Console.ReadLine());


                                while (!(value > 0 && value <= _producerRepository.GetAll().Count))
                                {
                                    Console.WriteLine("Select Producer from the list only:");
                                    value = Convert.ToInt32(Console.ReadLine());
                                }
                                _imdb.AddMovies(_nameOfMovie, _yearOfRelease, _plotOfMovie,_list,value);
                                Console.WriteLine("\n**************************************");
                                Console.WriteLine("*Movie successfully added to the list*");
                                Console.WriteLine("**************************************\n\n");

                            }
                            break;
                        }

                    case 3:
                        {
                            Console.WriteLine("----------------------------------------------");
                            Console.WriteLine("You entered 'Add an Actor'");
                            Console.WriteLine("==============================================\n\n");
                           
                            Console.Write("Name of the Actor: ");
                            String _nameOfActor = Console.ReadLine();
                           
                            if (_imdb.ValidateName(_nameOfActor))
                            {

                                Console.Write("Enter DOB of Actor (DD/MM/YY): ");
                                DateTime _dateOfBActor;
                                if (DateTime.TryParse(Console.ReadLine(), out _dateOfBActor))
                                {
                                    _imdb.AddActors(_nameOfActor,_dateOfBActor);
                                    Console.WriteLine("\n*************************");
                                    Console.WriteLine("*Actor Added succesfully*");
                                    Console.WriteLine("*************************\n\n");
                                }
                              
                                else
                                {
                                    Console.WriteLine("\n==>> DOB should be in (DD/MM/YYYY)\nPlease try again with valid date of birth <<==\n\n");                                   
                                }
                                
                            }
                            else
                            {
                                Console.WriteLine("\n==>>Enter the valid name <<==\n\n");
                            }
                            break;
                        }

                    case 4:
                        {
                            Console.WriteLine("----------------------------------------------");
                            Console.WriteLine("You entered 'Add a Producer'");
                            Console.WriteLine("==============================================\n\n");
                            
                            Console.Write("Name of Producer: ");
                            String _nameOfProducer = Console.ReadLine();
                            
                            if (_imdb.ValidateName(_nameOfProducer))
                            {


                                Console.Write("Enter DOB of Producer (DD/MM/YY): ");
                                DateTime _dateOfBProducer;
                                if (DateTime.TryParse(Console.ReadLine(), out _dateOfBProducer))
                                {
                                    _imdb.AddProducer(_nameOfProducer, _dateOfBProducer);
                                    Console.WriteLine("\n****************************");
                                    Console.WriteLine("*Producer added successfully*");
                                    Console.WriteLine("****************************\n\n");
                                }
                                
                                else
                                {
                                    
                                    Console.WriteLine("\n==>> DOB should br in (DD/MM/YYYY)\nPlease try again with valid date of birth <<==\n\n");
                                }
                            }
                            else
                            {
                                Console.WriteLine("\n==>>Enter valid name <<==\n\n");
                               
                            }
                            break;
                        }

                    case 5:
                        {
                            Console.WriteLine("----------------------------------------------");
                            Console.WriteLine("You entered 'Remove Movie from List'");
                            Console.WriteLine("==============================================\n\n");
                           
                            if (_movieRepository.GetAll().Count == 0)
                            {
                                Console.WriteLine("\n==>There are no movies to be deleted<<== \n\n");
                            }
                            else
                            {
                                Console.WriteLine("Movie Collection are: ");
                                foreach (var list in _movieRepository.GetAll())
                                {
                                    Console.WriteLine("  *" + list.Name);
                                }
                                Console.WriteLine("\n\n==============================================\n\n");


                                Console.Write("Enter the Movie name to Remove from the list: ");
                                var _movieName = Console.ReadLine();
                                if (_imdb.RemoveMovie(_movieName))
                                {
                                    Console.WriteLine("\n*********************************************************");
                                    Console.WriteLine("*  " + _movieName + "  Sucessfully deleted from the List  *");
                                    Console.WriteLine("*********************************************************\n\n");
                                }
                                else
                                {
                                    Console.WriteLine("\n==>>Enter Valid Movie Name<<== \n\n");
                                }
                            }
                            break;
                        }

                    case 6: Environment.Exit(0);break;

                    default: Console.WriteLine("Please Enter Valid Key");break;
                }
            } while (true);
            
        }
    }
}
