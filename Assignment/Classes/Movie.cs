﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment
{
    public class Movie         //Movie class stores the movie name, release date, plot, actors and producer.
    {
        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        
        public Producer Producer { get; set; }

        
    }
}
