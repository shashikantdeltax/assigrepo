﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment
{
    public class Producer                      //producer class stores the producer name and date of birth.
    {
        public string Name { get; set; }
        public DateTime Dob { get; set; }
    }
}
