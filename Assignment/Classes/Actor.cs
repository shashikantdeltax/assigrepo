﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment
{
    public class Actor                         //Actor class stores the producer name and date of birth.
    {
        public String Name { get; set; }
        public DateTime Dob { get; set; }
    }
}
