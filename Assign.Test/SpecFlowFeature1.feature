﻿Feature: IMDb Ratings 
	

@addMovie
Scenario: Add movie to the list
	Given A movie with name "Bahubali"
	And the year of release is "2015"
	And the movie plot is "In the kingdom of Mahishmati, while pursuing his love, Shivudu learns about the conflict ridden past of his family and his legacy. He must now prepare himself to face his newfound arch-enemy."
	
	And the actors of the movie are "1 2"
	
	And the producer of the movie is "1"
	When I add movie to the list
	Then the movie list should contains a movie like
	| Name     | YearOfRelease | Plot                                                                                                                                                                                            |
	| Bahubali | 2015          | In the kingdom of Mahishmati, while pursuing his love, Shivudu learns about the conflict ridden past of his family and his legacy. He must now prepare himself to face his newfound arch-enemy. | 
	Then the movie actors are
	| Name       | Dob       |
	| pravash    | 12/3/1980 |
	| bhalal dev | 5/8/1982  |   
	Then the movie producer is
	| Name             | Dob      |
	| Shobu Yarlagadda | 7/4/1988 |       
	

@listMovies
Scenario: List the movies
	Given Movies are there in the list
	When I fetch all the movies
	Then the list of movies contains are
	| Name     | YearOfRelease | Plot                                                                                                                                                                                                   |
	| Bahubali | 2015          | In the kingdom of Mahishmati, while pursuing his love, Shivudu learns about the conflict ridden past of his family and his legacy. He must now prepare himself to face his newfound arch-enemy.        |
	| Raavan   | 2010          | A bandit kidnaps an officer's wife, falls in love with her and lets her go, only to realise that she is in love with him. Realising this, her own husband uses her as a scapegoat to reach the bandit. |
	Then the movies actors are
	| Name       | Dob       |
	| pravash    | 12/3/1980 |
	| bhalal dev | 5/8/1982  |
	| pravash    | 12/3/1980 |
	Then the movies producers are
	| Name             | Dob      |
	| Shobu Yarlagadda | 7/4/1988 |
    | Shobu Yarlagadda | 7/4/1988 |