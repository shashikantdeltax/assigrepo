﻿using System;
using System.Collections.Generic;
using Assignment;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Assign.Test
{
    [Binding]
    public class IMDbRatingsSteps
    {
        
        List<Movie> _listMovies = new List<Movie>();
        MovieRepository _movieRepository = new MovieRepository();
        string _nameOfMovie;
        int _yearOfRelease;
        string _plotOfMovie;
        Movie _singleMovie;
        Producer _producer;
        List<Actor> _list=new List<Actor>();
        List<Producer> _listPro = new List<Producer>();
        int index;

        ActorRepository _actorRepository = new ActorRepository();


        [BeforeTestRun]
        public static void BeforeTestRunScenario()
        {
            ActorRepository _actorRepository = new ActorRepository();
            ProducerRepository _producerRepository = new ProducerRepository();
            MovieRepository _movieRepository = new MovieRepository();

            _actorRepository.AddNew(new Actor() { Name = "pravash", Dob = new DateTime(1980,12,03) });
            _actorRepository.AddNew(new Actor() { Name = "bhalal dev", Dob = new DateTime(1982,05,08) });

            _producerRepository.AddNew(new Producer() { Name = "Shobu Yarlagadda", Dob = new DateTime(1988,07,04) });


            _movieRepository.AddNew(new Movie() { Name = "Raavan", YearOfRelease = 2010, Plot = "A bandit kidnaps an officer's wife, falls in love with her and lets her go, only to realise that she is in love with him. Realising this, her own husband uses her as a scapegoat to reach the bandit.", Actors = new List<Actor>(){ _actorRepository.GetById(0) }, Producer = _producerRepository.GetById(0) });

        }

        [Given(@"A movie with name ""(.*)""")]
        public void GivenAMovieWithName(string p0)
        {
            _nameOfMovie = p0;
        }
        
        [Given(@"the year of release is ""(.*)""")]
        public void GivenTheYearOfReleaseIs(int  p0)
        {
            _yearOfRelease = p0;
        }
        
       

        [Given(@"the movie plot is ""(.*)""")]
        public void GivenTheMoviePlotIs(string p0)
        {
            _plotOfMovie = p0;
        }

        [Given(@"the actors of the movie are ""(.*)""")]
        public void GivenTheActorsOfTheMovieAre(string p0)
        {
            string[] tokens = p0.Split(' ');
            int[] numbers = Array.ConvertAll(tokens, int.Parse);
            foreach (var index in numbers)
            {
                  _list.Add(_actorRepository.GetById(index - 1));
            }
        }


        ProducerRepository _listProducer = new ProducerRepository();
        [Given(@"the producer of the movie is ""(.*)""")]
        public void GivenTheProducerOfTheMovieIs(string p0)
        {
            _producer = _listProducer.GetById(Convert.ToInt32(p0)-1);
            index = Convert.ToInt32(p0);
        }


        [When(@"I add movie to the list")]
        public void WhenIAddMovieToTheList()
        {
            _singleMovie = new Movie() { Name = _nameOfMovie, YearOfRelease = _yearOfRelease, Plot = _plotOfMovie, Actors = _list, Producer = _producer };
            _movieRepository.AddNew(_singleMovie);
        }



        [Then(@"the movie list should contains a movie like")]
        public void ThenTheMovieListShouldContainsAMovieLike(Table table)
        {
            table.CompareToInstance(_singleMovie);
        }




        [Then(@"the movie actors are")]
        public void ThenTheMovieActorsAre(Table table)
        {
            table.CompareToSet(_list);
        }

        [Then(@"the movie producer is")]
        public void ThenTheMovieProducerIs(Table table)
        {
            table.CompareToInstance(_listProducer.GetById(index - 1));
        }





        [Given(@"Movies are there in the list")]
        public void GivenMoviesAreThereInTheList()
        {
            
        }

        
        
        [When(@"I fetch all the movies")]
        public void WhenIFetchAllTheMovies()
        {
            _list.Clear();
            _listMovies = _movieRepository.GetAll();    //
            foreach(var list in _listMovies)
            {
                foreach (var actor in list.Actors)
                {
                    _list.Add(actor);
                }
                _listPro.Add(list.Producer);
            }

        }
        
        
        
        [Then(@"the list of movies contains are")]
        public void ThenTheListOfMoviesContainsAre(Table table)
        {
            table.CompareToSet(_listMovies);

        }

        [Then(@"the movies actors are")]
        public void ThenTheMoviesActorsAre(Table table)
        {
            table.CompareToSet(_list);
        }

        [Then(@"the movies producers are")]
        public void ThenTheMoviesProducersAre(Table table)
        {
            table.CompareToSet(_listPro);
        }
    }
}
